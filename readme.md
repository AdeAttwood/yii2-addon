# Yii Helpers

A collection of uefull helpers for the Yii2 framework

## Install

Install with composer

~~~php
compsoer install adeattwood/yii-addon
~~~

## Class docs

You can find the class docs [HERE](http://adeattwood.co.uk/open-source/yii-helpers/master)

