<?php

use yiiaddon\components\ActiveRecordCollection;
use yiiaddon\helpers\ArrayObject;
use yiiaddon\tests\_data\ActiveModel;

/**
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v0.1
 */
class ActiveRecordCollectionCest
{

    /**
     * The model collection built before every test
     *
     * @var ActiveRecordCollection
     */
    public $model;

    public function _before( UnitTester $I )
    {
        $models = [];

        for ($i  = 1; $i <= 10; $i++) {
            $models[$i] = new ActiveModel(['id' => $i]);
        }

        $this->model = new ActiveRecordCollection($models);
    }

    public function _after( UnitTester $I )
    {
    }

    public function testMap(UnitTester $I)
    {
        $maped = $this->model->map('id', 'propTwo');

        $I->assertEquals($maped[1], 'two');
        $I->assertEquals($maped->count(), 10);
        $I->assertTrue($maped instanceof ArrayObject);
    }

}
