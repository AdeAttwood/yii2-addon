<?php

use yiiaddon\helpers\ArrayHelper;
use yiiaddon\tests\_data\ModelOne;

/**
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v0.1
 */
class ArrayHelperCest
{

    public function _before( UnitTester $I )
    {
    }

    public function _after( UnitTester $I )
    {
    }

    public function testNumberArray( UnitTester $I )
    {
        $array = ArrayHelper::numberArray(10, 2, 2);

        $I->assertEquals($array[2], 2);
        $I->assertFalse(isset($array[3]));
        $I->assertEquals($array[4], 4);
    }

    public function testNumberArrayWithStep( UnitTester $I )
    {
        $array = ArrayHelper::numberArray(10);

        $I->assertEquals($array[0], 0);
        $I->assertEquals($array[10], 10);
        $I->assertFalse(isset($array[11]));
    }

    public function testParseAttribute( UnitTester $I )
    {
        $model = new ModelOne;

        $model->propOne = 'test';

        $data = ArrayHelper::parseAttribute($model, 'propOne');

        $I->assertEquals($data->format, 'text');

        $I->assertEquals($data->lable, 'Prop One');

        $I->assertEquals($data->value, 'test');
        $I->assertEquals($data->raw, 'test');
    }

    public function testErrorWithParseAttribute( UnitTester $I )
    {
        $model = new ModelOne;

        $model->propOne = 'test';
        try {
            $data = ArrayHelper::parseAttribute($model, 'not_a_prop');
            $I->fail('Not exseption thrown');
        } catch (\yii\base\UnknownPropertyException $e) {
            $I->assertEquals($e->getMessage(), 'Getting unknown property: yiiaddon\tests\_data\ModelOne::not_a_prop');
        } catch (\Exception $e) {
            $I->fail('\yii\base\UnknownPropertyException should have been thorwn');
        }
    }

    public function testInvalidConfigParseAttribute( UnitTester $I )
    {
        $model = new ModelOne;

        $model->propOne = 'test';
        try {
            $data = ArrayHelper::parseAttribute($model, '');
            $I->fail('Not exseption thrown');
        } catch (\yii\base\InvalidConfigException $e) {
            $I->assertEquals($e->getMessage(), 'The column must be specified in the format of "attribute", "attribute:format" or "attribute:format:label"');
        } catch (\Exception $e) {
            $I->fail('\yii\base\InvalidConfigException should have been thorwn');
        }
    }

}
