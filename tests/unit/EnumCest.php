<?php

use yiiaddon\helpers\Enum;

/**
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v0.1
 */
class EnumCest
{

    public function _before( UnitTester $I )
    {
    }

    public function _after( UnitTester $I )
    {
    }

    public function testNumberArray( UnitTester $I )
    {
        $I->assertEquals(Enum::formatBytes(28434322.25), '27.12 MB');
        $I->assertEquals(Enum::formatBytes(17328347842.25, 3), '16.138 GB');
    }

    /**
     * @todo test returning a resource
     */
    public function testGetTypeReturnValue( UnitTester $I )
    {
        $array = [
            'array' => [ 'key' => 'value' ],
            'null' => null,
            'boolean' => true,
            'float' => 1.2,
            'integer' => 3,
            'datetime' => '2-2-1989',
            'string' => 'I\'m a string',
            'unknown' => new StdClass()
        ];

        foreach ($array as $type => $var) {
            $I->assertEquals($type, Enum::getType($var));
        }
    }

}
