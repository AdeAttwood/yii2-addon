<?php

use yiiaddon\db\ActiveQuery;
use yiiaddon\tests\_data\ActiveModel;

/**
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v0.1
 */
class ActiveRecordCest
{

    /**
     * The instace of ActiveModel that we will be using in the tests
     *
     * @var ActiveModel
     */
    public $model;

    public function _before( UnitTester $I )
    {
        $this->model = new ActiveModel(['id' => 1]);
    }

    public function _after( UnitTester $I )
    {
    }

    public function testGetValue(UnitTester $I)
    {
        $value = $this->model->getValue('propOne');

        $I->assertEquals($value, 'one');
    }

    public function testGetValueAsDate(UnitTester $I)
    {
        $value = $this->model->getValue('date:date');
        $I->assertEquals($value, 'Sep 17, 2017');
    }

    public function testParseAttribute(UnitTester $I)
    {
        $value = $this->model->parseAttribute('date:date');

        $I->assertEquals($value->value, 'Sep 17, 2017');
        $I->assertEquals($value->raw, 1505655703);
        $I->assertEquals($value->format, 'date');
        $I->assertEquals($value->lable, 'Date');
    }

    public function testFind(UnitTester $I)
    {
        $query = ActiveModel::find();
        $I->assertTrue($query instanceof ActiveQuery);
    }

}
