<?php

use yiiaddon\helpers\ArrayObject;

/**
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v0.1
 */
class ArrayObjectCest
{
    public $arrayObject;

    public $array = [
        'key_one' => 'Value One',
        'key_two' => 'Value Two'
    ];

    public function _before( UnitTester $I )
    {
        $this->arrayObject = new ArrayObject($this->array);
    }

    public function _after( UnitTester $I )
    {
    }

    public function testArrayAsProps( UnitTester $I )
    {
        $I->assertEquals($this->arrayObject->key_one, 'Value One');
    }

    /**
     * undocumented function
     *
     * @return void
     */
    public function testArrayContains( UnitTester $I )
    {
        $I->assertTrue($this->arrayObject->contains('Value One'));
        $I->assertFalse($this->arrayObject->contains('Not in array'));
    }

    /**
     * undocumented function
     *
     * @return void
     */
    public function testToTable( UnitTester $I )
    {
        $table = $this->arrayObject->toTable();

        $I->assertContains('<table class="table table-bordered table-striped">', $table);
        $I->assertContains('<span title="string" style="cursor: default; border-bottom: 1px #aaa dashed;">Value One</span>', $table);
    }

    public function testRandomValue( UnitTester $I )
    {
        for($i = 0; $i < 5; $i++) {
            $I->assertContains($this->arrayObject->randomValue(), $this->array);
        }
    }

    public function testPrepend(UnitTester $I)
    {
        $this->arrayObject->prepend('New Value');

        $I->assertEquals($this->arrayObject[0], 'New Value');

        $this->arrayObject->prepend(['topProp' => 'Top Value']);

        $I->assertEquals($this->arrayObject->topProp, 'Top Value');
    }

    public function testToJson(UnitTester $I)
    {
        $json = $this->arrayObject->toJson();
        $I->assertEquals($json, '{"key_one":"Value One","key_two":"Value Two"}');
    }

    public function testChunk(UnitTester $I)
    {
        $this->arrayObject->combine([
            'key_three' => 'Value Three',
            'key_four'  => 'Value Four',
            'key_five'  => 'Value Five',
            'key_six'   => 'Value Six'
        ]);

        $chunks = $this->arrayObject->chunks(2);

        $I->assertEquals($chunks->count(), 3);

        $I->assertEquals($chunks->{0}->count(), 2);
        $I->assertEquals($chunks->{1}->count(), 2);
        $I->assertEquals($chunks->{2}->count(), 2);

        $I->assertTrue($chunks->{0}->contains('Value One'));
        $I->assertFalse($chunks->{0}->contains('Value Three'));

        $I->assertTrue($chunks->{1}->contains('Value Three'));
        $I->assertFalse($chunks->{1}->contains('Value One'));
        
        $I->assertTrue($chunks->{2}->contains('Value Five'));
        $I->assertFalse($chunks->{2}->contains('Value One'));
    }

}
