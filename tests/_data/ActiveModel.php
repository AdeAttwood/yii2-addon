<?php

namespace yiiaddon\tests\_data;

class ActiveModel extends \yiiaddon\db\ActiveRecord
{
    public $id;

    public $propOne   = 'one';
    public $propTwo   = 'two';
    public $propThree = 'three';
    public $propFour  = 'four';
    public $propFive  = 'five';

    public $date = 1505655703;
}
