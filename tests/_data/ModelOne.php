<?php

namespace yiiaddon\tests\_data;

/**
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v0.1
 */
class ModelOne extends \yii\base\Model
{

    /**
     * The first test proprety
     *
     * @var string
     */
    public $propOne;

    /**
     * The validation rules for the model
     *
     * @return array
     */
    public function rules()
    {
        return [
            [ [ 'propOne' ], 'string' ]
        ];
    }

}
