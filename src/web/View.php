<?php

namespace yiiaddon\web;

use Yii;
use yii\helpers\Url;

/**
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v1.2
 */
class View extends \yii\web\View
{
    /**
     * If to add facebook og tags to the head
     *
     * @var boolean
     */
    public $facebook_og = true;

    /**
     * If to add twitter cards to the head tag
     *
     * @var boolean
     */
    public $twitter_card = true;

    /**
     * Description of the page
     *
     * @var string
     */
    public $description;

    /**
     * The url of the page
     *
     * This will be used in the canonical tag
     *
     * @var string
     */
    public $url;

    /**
     * Keyword to be put in the kewords meata tag
     *
     * @var string
     */
    public $keywords;

    /**
     * Url to the image to be used in the og and card metas
     *
     * @var string
     */
    public $image;

    /**
     * The twitter name e.g. '@twitter'
     *
     * @var string
     */
    public $twitterTag;

    /**
     * The page authers name
     *
     * @var string
     */
    public $authorName;

    /**
     * The page authers twitter tag
     *
     * @var string
     */
    public $authorTwitterTag;

    /**
     * The default viewport content used in the viewport meta tag
     *
     * @var string
     */
    public $viewportContent = 'width=device-width, initial-scale=1';

    /**
     * Googel analytics code to put in the js
     *
     * @var string
     */
    public $googleAnalyticsCode;

    /**
     * The position to put the google analytics js
     *
     * Default is `POS_END`
     *
     * @var integer
     */
    public $googleAnalyticsPosition;

    /**
     * The url to the google analytics js
     *
     * @var string
     */
    public $googleAnalyticsFile = 'https://www.google-analytics.com/analytics.js';

    /**
     * The js variable the store the google analytics js object in
     *
     * e.g. `ga('send', 'pageview');`
     *
     * @var string
     */
    public $googleAnalyticsVar = 'ga';

    /**
     * Google analytics vars to set
     *
     * @var array
     *
     * @see https://developers.google.com/analytics/devguides/collection/analyticsjs/command-queue-reference#set
     */
    protected $googleAnalyticsSet = [];

    /**
     * Initializes the view instance
     *
     * @return void
     */
    public function init()
    {
        if (!$this->googleAnalyticsPosition) {
            $this->googleAnalyticsPosition = self::POS_END;
        }

        return parent::init();
    }

    /**
     * Adds a set variable to the google analyitcs script
     *
     * @see https://developers.google.com/analytics/devguides/collection/analyticsjs/command-queue-reference#set
     *
     * @param string $key   The var key
     * @param mixed  $value The var value
     *
     * @return void
     */
    public function googleAnalyticsSet($key, $value)
    {
        $this->googleAnalyticsSet[$key] = $value;
    }

    /**
     * Renders the head html adding all of the registers scripts and adding the meta tags
     *
     * @return string
     */
    public function renderHeadHtml()
    {
        $metas = [
            'charset' => ['charset' => Yii::$app->charset],
            'viewport' => ['name' => 'viewport', 'content' => $this->viewportContent],
            'http-equiv' => ['http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge'],
            'description' => ['name' => 'description', 'content' => $this->description],
            'keywords' => $this->keywords ? ['name' => 'keywords', 'content' => $this->keywords] : false,
            'author' => $this->authorName ? ['name' => 'author', 'content' => $this->authorName] : false
        ];

        $links = [
            'canonical' => ['rel' => 'canonical', 'href' => Url::to($this->url, true)]
        ];

        if ($this->facebook_og) {
            $metas = array_merge($metas, $this->registerFacebookOg());
        }

        if ($this->twitter_card) {
            $metas = array_merge($metas, $this->registerTwitterCard());
        }

        foreach ($metas as $meta => $content) {
            if (!isset($this->metaTags[$meta]) && $content) {
                $this->registerMetaTag($content, $meta);
            }
        }

        foreach ($links as $link => $content) {
            if (!isset($this->metaTags[$link]) && $content) {
                $this->registerLinkTag($content, $link);
            }
        }

        if ($this->googleAnalyticsCode) {
            $this->registerGoogleAnalytics();
        }

        return '<title>'.$this->title.'</title>'.parent::renderHeadHtml();
    }

    /**
     * Creates and registers the google analytics scripts
     *
     * @return void
     */
    public function registerGoogleAnalytics()
    {
        $js  = "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','$this->googleAnalyticsFile','$this->googleAnalyticsVar');";
        $js .= "$this->googleAnalyticsVar('create', '$this->googleAnalyticsCode', 'auto');";
        $js .= "$this->googleAnalyticsVar('send', 'pageview');";

        foreach ($this->googleAnalyticsSet as $key => $value) {
            $js .= "$this->googleAnalyticsVar('set', '$key', '$value');";
        }

        $this->registerJS($js, $this->googleAnalyticsPosition);
    }

    /**
     * Adds the facebook og meta tags
     *
     * @return array
     */
    public function registerFacebookOg()
    {
        return [
            'og:locale' => ['property' => 'og:locale', 'content' => 'en_GB'],
            'og:type' => ['property' => 'og:type', 'content' => 'website'],
            'og:title' => ['property' => 'og:title', 'content' => $this->title],
            'og:description' => ['property' => 'og:description', 'content' => $this->description],
            'og:url' => ['property' => 'og:url', 'content' => Url::to($this->url, true)],
            'og:site_name' => ['property' => 'og:site_name', 'content' => Yii::$app->name],
            'og:image' => $this->image ? ['property' => 'og:image', 'content' => Url::to($this->image, true)] : false,
        ];
    }

    /**
     * Adds the twitter card meta tags
     *
     * @return array
     */
    public function registerTwitterCard()
    {
        return [
            'twitter:card' => ['name' => 'twitter:card', 'content' => 'summary'],
            'twitter:description' => ['name' => 'twitter:description', 'content' => $this->description],
            'twitter:title' => ['name' => 'twitter:title', 'content' => $this->title],
            'twitter:url' => ['name' => 'twitter:url', 'content' => Url::to($this->url, true)],
            'twitter:site' =>  $this->twitterTag ? ['name' => 'twitter:site', 'content' => $this->twitterTag] : false,
            'twitter:image' => $this->image ? ['name' => 'twitter:image', 'content' => Url::to($this->image, true)] : false,
            'twitter:creator' => $this->authorTwitterTag ? ['name' => 'twitter:creator', 'content' => $this->authorTwitterTag] : false,
        ];
    }

}
