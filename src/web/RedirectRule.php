<?php

namespace yiiaddon\web;

use Yii;
use yii\helpers\Url;

/**
 * [
 *     'class' => 'yiiaddon\web\RedirectRule',
 *
 *     'route' => '/about',
 *
 *     'permanents' => [
 *         'deprecated-about',
 *         'an-older-deprecated-about'
 *     ],
 *
 *     'temporaries' => [
 *         'under-construction-about',
 *     ]
 * ]
 *
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v1.2
 */
class RedirectRule extends \yii\web\UrlRule
{
    public $permanents = [];

    public $temporaries = [];

    public function init()
    {
        if ($this->pattern === null) {
            $this->pattern = false;
        }

        if ($this->name === null) {
            $this->name = $this->route;
        }
        
        parent::init();
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->getPathInfo();

        if(in_array($pathInfo, $this->permanents)) {
            $request->setPathInfo($this->name);
            Yii::$app->response->redirect($this->name, 301);
        } elseif(in_array($pathInfo, $this->temporaries)) {
            $request->setPathInfo($this->name);
            Yii::$app->response->redirect($this->name, 302);
        }

        parent::parseRequest($manager, $request);
        Yii::$app->end();
    }

}
