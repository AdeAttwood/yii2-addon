<?php

namespace yiiaddon\behaviors;

use yii\db\BaseActiveRecord;
use yii\web\UploadedFile;

/**
 * Loads the Uploaded file instance to an attribute in your model
 *
 * ~~~php
 * use yiiaddon\behaviors\UploadedFileBehavior;
 *
 * public function behaviors()
 * {
 *     return [
 *         [
 *             'class' => UploadedFileBehavior::className(),
 *             'fileAttribute' => 'my_file'
 *         ],
 *     ];
 * }
 * ~~~
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v1.2
 */
class UploadedFileBehavior extends \yii\behaviors\AttributeBehavior
{
    
    /**
     * The attribute in your model that will be uploading the file from the form
     *
     * @var string
     */
    public $fileAttribute = 'file';
    
    /**
     * The temporary instance of UploadFile
     *
     * @var yii\web\UploadedFile
     */
    public $value;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (empty($this->attributes)) {
            $this->attributes = [
                BaseActiveRecord::EVENT_BEFORE_VALIDATE => $this->fileAttribute,
            ];
        }
    }
    
    /**
     * @inheritdoc
     */
    protected function getValue($event)
    {
        if ($this->value === null) {
            return UploadedFile::getInstance($this->owner, $this->fileAttribute);
        }

        return parent::getValue($event);
    }

}
