<?php

namespace yiiaddon\helpers;

use Yii;
use yii\helpers\Html;

use yiiaddon\helpers\Enum;

/**
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v0.1
 */
class ArrayObject extends \ArrayObject
{

    /**
     * __construct
     *
     * @param mixed  $array          The array to put into the array object
     * @param mixed  $flags          The falgs of the object
     * @param string $iterator_class The iterator class
     *
     * @return void
     */
    public function __construct( $array = [], $flags = null, $iterator_class = 'ArrayIterator' )
    {
        if($flags === null) {
            $flags = self::ARRAY_AS_PROPS;
        }

        parent::__construct($array, $flags, $iterator_class);
    }

    /**
     * Test that the ArrayObject contains an item
     *
     * @param mixed $item The item to test
     *
     * @return bool
     */
    public function contains( $item )
    {
        $flipped = array_flip(( array ) $this);
        return isset($flipped[$item]);
    }

    /**
     * Gets a random value from the array
     *
     * @param int $count How many items to return
     *
     * @return int|self
     */
    public function randomValue( $count = 1 )
    {
        $arr = ( array ) $this;

        shuffle($arr);

        $r = array();
        for ($i = 0; $i < $count; $i++) {
            $r[] = $arr[$i];
        }
        return $count == 1 ? $r[0] : new self($r);
    }

    /**
     * Adds an item to the start of the object
     *
     * @param mixed $value The item to prepend
     *
     * @return void
     */
    public function prepend($value)
    {
        $array = $this->getArrayCopy();

        if (is_string($value)) {
            $value = (array) $value;
        }

        if (is_object($value)) {
            $value = [$value];
        }
        $this->exchangeArray($value + $array);
    }

    /**
     * Combines an array to the existing object
     *
     * @param array $array The array to comnine
     *
     * @return void
     */
    public function combine($array)
    {
        $this->exchangeArray(array_merge((array) $this->getArrayCopy(), $array));
    }

    /**
     * Splits the object up into chunks
     *
     * Example:
     *
     * ~~~php
     * $obj = new ArrayObject([
     *     'key_one'   => 'Value One'
     *     'key_two'   => 'Value Two'
     *     'key_three' => 'Value Three'
     *     'key_four'  => 'Value Four'
     *     'key_five'  => 'Value Five'
     *     'key_six'   => 'Value Six'
     * ]);
     *
     * $chunks = $obj->chunks(2);
     *
     * [
     *     [
     *         'key_one' => 'Value One'
     *         'key_two' => 'Value Two'
     *     ],
     *     [
     *         'key_three' => 'Value Three'
     *         'key_four'  => 'Value Four'
     *     ],
     *     [
     *         'key_five' => 'Value Five'
     *         'key_six'  => 'Value Six'
     *     ],
     * ]
     * ~~~
     *
     * @param integer $count How big to make the chunks
     *
     * @return void
     */
    public function chunks($count)
    {
        $out   = [];
        $array = array_chunk((array) $this, $count);

        foreach ($array as $chunk) {
            $out[] = new self($chunk);
        }

        return new self($out);
    }

    /**
     * Turns the object into a json stirng
     *
     * @return string
     */
    public function toJson()
    {
        return \yii\helpers\Json::encode((array) $this);
    }

    /**
     * Converts the ArrayObject into a table
     *
     * @param mixed  $transpose
     * @param mixed  $recursive
     * @param mixed  $typeHint
     * @param string $tableOptions
     * @param mixed  $keyOptions
     * @param string $valueOptions
     * @param string $null
     *
     * @return string The html for the table
     */
    public function toTable( $transpose = false, $recursive = false, $typeHint = true, $tableOptions = [ 'class' => 'table table-bordered table-striped' ], $keyOptions = [], $valueOptions = [ 'style' => 'cursor: default; border-bottom: 1px #aaa dashed;' ], $null = '<span class="not-set">(not set)</span>' )
    {
        if (empty(( array ) $this)) {
            return false;
        }

        $array = ( array ) $this;

        // Start the table
        $table = Html::beginTag('table', $tableOptions)."\n";
        // The header
        $table .= "\t<tr>";
        if ($transpose) {
            foreach ($this as $key => $value) {
                if ($typeHint) {
                    $valueOptions['title'] = Enum::getType(strtoupper($value));
                }
                if (is_array($value)) {
                    $value = '<pre>'.print_r($value, true).'</pre>';
                } else {
                    $value = Html::tag('span', $value, $valueOptions);
                }
                $table .= "\t\t<th>".Html::tag('span', $key, $keyOptions).'</th>'.'<td>'.$value."</td>\n\t</tr>\n";
            }
            $table .= '</table>';
            return $table;
        }
        if (!isset($array[0]) || !is_array($array[0])) {
            $array = array( $array );
        }
        // Take the keys from the first row as the headings
        foreach (array_keys($array[0]) as $heading) {
            $table .= '<th>'.Html::tag('span', $heading, $keyOptions).'</th>';
        }
        $table .= "</tr>\n";
        // The body
        foreach ($array as $row) {
            $table .= "\t<tr>";
            foreach ($row as $cell) {
                $table .= '<td>';
                // Cast objects
                if (is_object($cell)) {
                    $cell = (array) $cell;
                }
                if ($recursive === true && is_array($cell) && !empty($cell)) {
                    // Recursive mode
                    $table .= "\n".static::array2table($cell, true, true)."\n";
                } else {
                    if ($cell !== null && is_bool($cell)) {
                        $val  = $cell ? 'true' : 'false';
                        $type = 'boolean';
                    } else {
                        $chk  = ( strlen($cell) > 0 );
                        $type = $chk ? Enum::getType($cell) : 'null';
                        $val  = $chk ? htmlspecialchars((string) $cell) : $null;
                    }
                    if ($typeHint) {
                        $valueOptions['title'] = $type;
                    }
                    $table .= Html::tag('span', $val, $valueOptions);
                }
                $table .= '</td>';
            }
            $table .= "</tr>\n";
        }
        $table .= '</table>';
        return $table;
    }

}
