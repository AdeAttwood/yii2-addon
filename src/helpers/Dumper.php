<?php

namespace adeattwood\helpers;

use Symfony\Component\VarDumper\VarDumper;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;

/**
 * Description of Dumper
 *
 * @author ade
 */
class Dumper
{

    public static $styles = array(
        'default' => 'background-color:#18171B; color:#FF8400; line-height:1.2em; font:14px Menlo, Monaco, Consolas, monospace; word-wrap: break-word; white-space: pre-wrap; position:relative; z-index:99999; word-break: normal',
        'num' => 'font-weight:bold; color:#1299DA',
        'const' => 'font-weight:bold',
        'str' => 'font-weight:bold; color:#56DB3A',
        'note' => 'color:#1299DA',
        'ref' => 'color:#A0A0A0',
        'public' => 'color:#FFFFFF',
        'protected' => 'color:#FFFFFF',
        'private' => 'color:#FFFFFF',
        'meta' => 'color:#B729D9',
        'key' => 'color:#56DB3A',
        'index' => 'color:#1299DA',
        'ellipsis' => 'color:#FF8400',
    );

    public static $displayOptions = array(
        'maxDepth' => 1,
        'maxStringLength' => 160,
        'fileLinkFormat' => null,
    );

    public static function dump( $var ) {
        VarDumper::setHandler(function ( $var ) {
            $cloner = new VarCloner();
            $dumper = 'cli' === PHP_SAPI ? new CliDumper() : new HtmlDumper();

            $dumper->setStyles(self::$styles);
            $dumper->setDisplayOptions(self::$displayOptions);

            $dumper->dump($cloner->cloneVar($var));
        });

        dump($var);
    }

    public static function dDump( $var )
    {
        self::dump($var);
        die;
    }

    public static function dumpInfo( $var )
    {
        $dataArray = [];

        $class = new \ReflectionClass($var);

        $dataArray['name'] = $class->name;

        $methods                = $class->getMethods();
        $dataArray['constants'] = ( array ) $class->getConstants();

        foreach ($class->getProperties() as $propertie) {
            $prop = '';

            if ($propertie->isPublic()) {
                $prop .= 'public';
            } elseif ($propertie->isPrivate()) {
                $prop .= 'private';
            } elseif ($propertie->isProtected()) {
                $prop .= 'protected';
            }

            if ($propertie->isStatic()) {
                $prop .= ' static';
            }

            $prop .= " \${$propertie->name}";

            $dataArray['properties'][] = $prop;
        }


        foreach ($methods as $method) {
            $prop = '';

            if ($method->isPublic()) {
                $prop .= 'public';
            } elseif ($method->isPrivate()) {
                $prop .= 'private';
            } elseif ($method->isProtected()) {
                $prop .= 'protected';
            }

            $prop .= " {$method->name}( ";

            $params = '';
            foreach ($method->getParameters() as $parameter) {
                $params .= "\${$parameter->name}";

                if ($parameter->isDefaultValueAvailable()) {
                    $value = $parameter->getDefaultValue();

                    if (is_array($value)) {
                        $value = '[]';
                    } elseif (is_bool($value)) {
                        $value = $value ? 'true' : 'false';
                    }

                    $params .= "= {$value}";

//                    self::dDump( $parameter->getDefaultValue() );
                }
                $params .= ', ';
            }

            $params = rtrim($params, ', ');
            $prop  .= "{$params} )";

//            $prop .= " : {$method->getReturnType()}";

            $dataArray['methords'][$prop] = [
                'file' => $method->getFileName(),
                'line' => $method->getStartLine(),
                'fileLine' => "{$method->getFileName()}:{$method->getStartLine()}",
                'doc' => $method->getDocComment()
            ];
        }

        sort($dataArray['properties']);
        ksort($dataArray['constants']);
        ksort($dataArray['methords']);
        self::dDump($dataArray);
    }

    public static function dDumpInfo( $var )
    {
        self::dumpInfo($var);
        die;
    }

}
