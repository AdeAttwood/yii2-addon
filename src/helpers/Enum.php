<?php

namespace yiiaddon\helpers;

/**
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v0.1
 */
class Enum extends \yii\base\Object
{
    const TYPE_ARRAY    = 'array';
    const TYPE_NULL     = 'null';
    const TYPE_BOOL     = 'boolean';
    const TYPE_FLOAT    = 'float';
    const TYPE_INT      = 'integer';
    const TYPE_DATETIME = 'datetime';
    const TYPE_STRING   = 'string';
    const TYPE_RESOURCE = 'resource';
    const TYPE_UNKNOWN  = 'unknown';

   /**
    *
     * Parses and returns a variable type
     *
     * Example:
     *
     * ~~~php
     * $data = [
     *     'id' => 1,
     *     'name' => 'Smith',
     *     'date' => '2014/01/22',
     *     'amount' => '4,323.23',
     *     'relations' => ['spouse', 'children']
     * ];
     * foreach ($data as $k=>$v) {
     *     echo "<b>$k</b>: " . Enum::getType($v) . "<br>";
     * }
     * ~~~
     *
     * @param string $var the variable to be parsed
     *
     * @return string
     */
    public static function getType( $var )
    {
        if ($var === null) {
            return self::TYPE_NULL;
        }
        
        if (is_array($var)) {
            return self::TYPE_ARRAY;
        }
        
        if (is_bool($var)) {
            return self::TYPE_BOOL;
        }
        
        if (is_float($var)
        || (!is_object($var) &&
             is_numeric(str_replace(',', '', $var)) && strpos($var, '.') > 0 && is_float(( float ) str_replace(',', '', $var)))
        ) {
            return self::TYPE_FLOAT;
        }
        
        if (is_int($var) || ( is_numeric($var) && is_int(( int ) $var) )) {
            return self::TYPE_INT;
        }
        
        if (is_scalar($var) && strtotime($var) !== false) {
            return self::TYPE_DATETIME;
        }

        if (is_scalar($var)) {
            return self::TYPE_STRING;
        }
                
        return self::TYPE_UNKNOWN;
    }

    /**
     * Check if a variable is empty or not set.
     *
     * Example:
     *
     * ~~~php
     * if (Enum::isEmpty([]) {
     *    echo 'Not empty';
     * }
     * ~~~
     *
     * @param mixed $var variable to perform the check
     *
     * @return boolean
     */
    public static function isEmpty( $var )
    {
        return !isset($var) ? true : ( is_array($var) ? empty($var) : ( $var === null || $var === '' ) );
    }

    /**
     * Format and convert "bytes" to its optimal higher metric unit.
     *
     * Example:
     *
     * ~~~php
     * echo Enum::formatBytes(120.32); // returns: 1.17 KB
     * echo Enum::formatBytes(28434322.25); // returns: 27.12 MB
     * echo Enum::formatBytes(17328347842.25, 3); // returns: 16.138 GB
     * ~~~
     *
     * @param double  $bytes     number of bytes
     * @param integer $precision the number of decimal places to round off
     *
     * @return string
     */
    public static function formatBytes( $bytes, $precision = 2 )
    {
        $units  = [ 'B', 'KB', 'MB', 'GB', 'TB' ];
        $bytes  = max($bytes, 0);
        $pow    = floor(( $bytes ? log($bytes) : 0 ) / log(1024));
        $pow    = min($pow, count($units) - 1);
        $bytes /= pow(1024, $pow);
        return round($bytes, $precision).' '.$units[$pow];
    }

}
