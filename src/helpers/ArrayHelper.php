<?php

namespace yiiaddon\helpers;

use Yii;
use yii\base\InvalidConfigException;

use yiiaddon\helpers\ArrayObject;
use yiiaddon\helpers\Enum;

/**
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v0.1
 */
class ArrayHelper extends \yii\helpers\ArrayHelper
{

    /**
     * Creates a array of numbers
     *
     * ~~~php
     * ArrayHelper::numberArray( 5, 2, 2 );
     *
     * array(5) {
     *     [2] => int(2)
     *     [4] => int(4)
     *     [6] => int(6)
     *     [8] => int(8)
     *     [10]=> int(10)
     * }
     * ~~~
     *
     * @param int $max  The max number in the array
     * @param int $min  The min mumber in the array
     * @param int $step The step for the itarator
     *
     * @return array
     */
    public static function numberArray( $max, $min = 0, $step = 1 )
    {
        $array = [];
        for($i = $min; $i <= $max; $i += $step) {
            $array[$i] = $i;
        }
        return $array;
    }

    /**
     * Gets the value form the model and returns a config of the arrtibute
     *
     * The function will return an ArrayObject with four attributes
     * - format The format of the attribute.
     * - value  The formatted value of the attribute.
     * - lable  The lable of the attribute form the model
     * - raw    The value of the attribute with no formatting
     *
     * @param mixed  $model     The model of the attribute to parse
     * @param string $attribute The you want to parse
     * @param array  $defalts   Array of defalt values
     *
     * @return ArrayObject The parsed config
     */
    public static function parseAttribute( $model, $attribute, $defalts = [] )
    {
        if (!preg_match('/^([^:]+)(:(\w*))?(:(.*))?$/', $attribute, $matches)) {
            throw new InvalidConfigException('The column must be specified in the format of "attribute", "attribute:format" or "attribute:format:label"');
        }

        $rawValue = self::getValue($model, $matches[1]);

        $format = isset($matches[3]) ? $matches[3] : self::remove($defalts, 'format', 'text');
        $value  = Enum::isEmpty($rawValue) ? self::remove($defalts, 'value', '( NOT SET )') : Yii::$app->formatter->format($rawValue, $format);
        $lable  = isset($matches[5]) ? $matches[5] : $model->getAttributeLabel($matches[1]);

        return new ArrayObject([
            'format' => $format,
            'value'  => $value,
            'lable'  => $lable,
            'raw'    => $rawValue
        ]);
    }
    
}
