<?php

namespace yiiaddon\filters;

use Yii;
use yii\di\Instance;
use yii\web\Response;

/**
 * Minifies the html output of the page
 *
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v1.2
 */
class PageMin extends \yii\base\ActionFilter
{
    /**
     * The the filter should be enabled
     *
     * @var boolean
     */
    public $enabled = true;
    
    /**
     * @inheritDoc
     */
    public function beforeAction($action)
    {
        if (!$this->enabled) {
            return true;
        }

        $response = Yii::$app->getResponse();
        $response->on(Response::EVENT_BEFORE_SEND, [$this, 'pageMin']);
        return true;
    }

    /**
     * Minifies the event data
     *
     * @param Event $event The request event
     *
     * @return void
     */
    public function pageMin($event)
    {
        Yii::$app->response->data = $this->minifyHtml($event->sender->data);
    }

    /**
     * Minifies html string
     *
     * @param string $input The string of html
     *
     * @return string The output html
     */
    public function minifyHtml( $input )
    {
        if(trim($input) === '') { return $input;
        }
        $input = preg_replace_callback('#<([^\/\s<>!]+)(?:\s+([^<>]*?)\s*|\s*)(\/?)>#s', function($matches) {
            return '<'.$matches[1].preg_replace('#([^\s=]+)(\=([\'"]?)(.*?)\3)?(\s+|$)#s', ' $1$2', $matches[2]).$matches[3].'>';
        }, str_replace("\r", '', $input));
        if(strpos($input, ' style=') !== false) {
            $input = preg_replace_callback('#<([^<]+?)\s+style=([\'"])(.*?)\2(?=[\/\s>])#s', function($matches) {
                return '<'.$matches[1].' style='.$matches[2].$this->minifyCss($matches[3]).$matches[2];
            }, $input);
        }
        if(strpos($input, '</style>') !== false) {
            $input = preg_replace_callback('#<style(.*?)>(.*?)</style>#is', function($matches) {
                return '<style'.$matches[1].'>'.$this->minifyCss($matches[2]).'</style>';
            }, $input);
        }
        if(strpos($input, '</script>') !== false) {
            $input = preg_replace_callback('#<script(.*?)>(.*?)</script>#is', function($matches) {
                return '<script'.$matches[1].'>'.$this->minifyJs($matches[2]).'</script>';
            }, $input);
        }
        return preg_replace(
            [
                '#<(img|input)(>| .*?>)#s',
                '#(<!--.*?-->)|(>)(?:\n*|\s{2,})(<)|^\s*|\s*$#s',
                '#(<!--.*?-->)|(?<!\>)\s+(<\/.*?>)|(<[^\/]*?>)\s+(?!\<)#s',
                '#(<!--.*?-->)|(<[^\/]*?>)\s+(<[^\/]*?>)|(<\/.*?>)\s+(<\/.*?>)#s',
                '#(<!--.*?-->)|(<\/.*?>)\s+(\s)(?!\<)|(?<!\>)\s+(\s)(<[^\/]*?\/?>)|(<[^\/]*?\/?>)\s+(\s)(?!\<)#s',
                '#(<!--.*?-->)|(<[^\/]*?>)\s+(<\/.*?>)#s',
                '#<(img|input)(>| .*?>)<\/\1>#s',
                '#(&nbsp;)&nbsp;(?![<\s])#',
                '#(?<=\>)(&nbsp;)(?=\<)#',
                '#\s*<!--(?!\[if\s).*?-->\s*|(?<!\>)\n+(?=\<[^!])#s'
            ],
            [
                '<$1$2</$1>',
                '$1$2$3',
                '$1$2$3',
                '$1$2$3$4$5',
                '$1$2$3$4$5$6$7',
                '$1$2$3',
                '<$1$2',
                '$1 ',
                '$1',
                ''
            ],
            $input
        );
    }

    /**
     * Minifies css strings inside the html
     *
     * @param string $input The input html
     *
     * @return string The output html
     */
    public function minifyCss($input)
    {
        if (trim($input) === '') {
            return $input;
        }

        return preg_replace(
            [
                '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')|\/\*(?!\!)(?>.*?\*\/)|^\s*|\s*$#s',
                '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/))|\s*+;\s*+(})\s*+|\s*+([*$~^|]?+=|[{};,>~+]|\s*+-(?![0-9\.])|!important\b)\s*+|([[(:])\s++|\s++([])])|\s++(:)\s*+(?!(?>[^{}"\']++|"(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')*+{)|^\s++|\s++\z|(\s)\s+#si',
                '#(?<=[\s:])(0)(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)#si',
                '#:(0\s+0|0\s+0\s+0\s+0)(?=[;\}]|\!important)#i',
                '#(background-position):0(?=[;\}])#si',
                '#(?<=[\s:,\-])0+\.(\d+)#s',
                '#(\/\*(?>.*?\*\/))|(?<!content\:)([\'"])([a-z_][a-z0-9\-_]*?)\2(?=[\s\{\}\];,])#si',
                '#(\/\*(?>.*?\*\/))|(\burl\()([\'"])([^\s]+?)\3(\))#si',
                '#(?<=[\s:,\-]\#)([a-f0-6]+)\1([a-f0-6]+)\2([a-f0-6]+)\3#i',
                '#(?<=[\{;])(border|outline):none(?=[;\}\!])#',
                '#(\/\*(?>.*?\*\/))|(^|[\{\}])(?:[^\s\{\}]+)\{\}#s'
            ],
            [
                '$1',
                '$1$2$3$4$5$6$7',
                '$1',
                ':0',
                '$1:0 0',
                '.$1',
                '$1$3',
                '$1$2$4$5',
                '$1$2$3',
                '$1:0',
                '$1$2'
            ],
            $input
        );
    }

    /**
     * Minifies js strings inside the html
     *
     * @param string $input The input html
     *
     * @return string The output html
     */
    public function minifyJs($input)
    {
        if (trim($input) === '') {
            return $input;
        }

        return preg_replace(
            [
                '#\s*("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')\s*|\s*\/\*(?!\!|@cc_on)(?>[\s\S]*?\*\/)\s*|\s*(?<![\:\=])\/\/.*(?=[\n\r]|$)|^\s*|\s*$#',
                '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/)|\/(?!\/)[^\n\r]*?\/(?=[\s.,;]|[gimuy]|$))|\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s*#s',
                '#;+\}#',
                '#([\{,])([\'])(\d+|[a-z_][a-z0-9_]*)\2(?=\:)#i',
                '#([a-z0-9_\)\]])\[([\'"])([a-z_][a-z0-9_]*)\2\]#i'
            ],
            [
                '$1',
                '$1$2',
                '}',
                '$1$3',
                '$1.$3'
            ],
            $input
        );
    }

}
