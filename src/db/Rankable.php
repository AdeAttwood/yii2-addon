<?php

namespace yiiaddon\db;

use Yii;
use yii\db\Expression;

/**
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v1.2
 */
trait Rankable
{

    public $rank_filed = 'rank';

    public $rank_group = false;

    public function getRankField()
    {
        return 'rank';
    }

    public function getRankGoup()
    {
        return false;
    }

    public function getNextRank()
    {
        $query = self::find();

        if ($this->rankGoup) {
            $query->where([$this->rankGroup => $this->{$this->rankGroup}]);
        }
        
        return $query->count();
    }

    public function changeRank( $new_rank )
    {
        $rank_filed = $this->rankField;
        $group      = $this->rankGoup;
        $old_rank   = $this->{ $rank_filed };

        if ($new_rank == $old_rank) {
            return true;
        }

        $value = '';

        if ($new_rank > $old_rank) {
            $value = $rank_filed.' - 1';
            $where = $rank_filed.'>= :rank1 AND '.$rank_filed.' <= :rank2';
        } else {
            $value = $rank_filed.' + 1';
            $where = $rank_filed.' <= :rank1 AND '.$rank_filed.' >= :rank2';
        }

        if ($group) {
            $where .= ' AND '.$group.' = '.$this->{ $group };
        }

        $query = Yii::$app->db->createCommand()
            ->update(self::tableName(), [ $rank_filed => new Expression($value) ], $where)
            ->bindValue(':rank1', $old_rank)
            ->bindValue(':rank2', $new_rank)
            ->execute();

        $this->updateAttributes([ 'rank' => $new_rank ]);

        return true;
    }

}
