<?php

namespace yiiaddon\db;

use Yii;
use yiiaddon\helpers\ArrayHelper;

/**
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v1.2
 */
class ActiveRecord extends \yii\db\ActiveRecord
{

    /**
     * Creates an instance of ActiveQuery
     *
     * @return yiiaddon\db\ActiveQuery
     */
    public static function find()
    {
        return Yii::createObject(ActiveQuery::className(), [get_called_class()]);
    }

    /**
     * Parses an attribute
     *
     * @see yiiaddon\helpers\ArrayHelper::parseAttribute
     *
     * @param string $attribute The attribute format string
     * @param array  $defalts   The array of defalts to override
     *
     * @return yiiaddon\helpers\ArrayObject
     */
    public function parseAttribute($attribute, $defalts = [])
    {
        return ArrayHelper::parseAttribute($this, $attribute, $defalts);
    }

    /**
     * Get a value of an attribute and formats with a string
     *
     * @see yiiaddon\helpers\ArrayHelper::parseAttribute
     *
     * @param string $attribute The attribute and format string
     * @param string $defalt    The defalt value to return if the attribute is not set
     *
     * @return mixed
     */
    public function getValue($attribute, $defalt = '( NOT SET )')
    {
        return $this->parseAttribute($attribute, ['value' => $defalt])->value;
    }

}
