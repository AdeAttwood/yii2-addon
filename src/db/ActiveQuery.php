<?php

namespace yiiaddon\db;

use yiiaddon\components\ActiveRecordCollection;

/**
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v1.2
 */
class ActiveQuery extends \yii\db\ActiveQuery
{

    /**
     * Overrides the Yii method to return an ActiveRecordCollection instead of an array
     *
     * @param mixed $db The DB connection used to create the DB command.
     *
     * @return ActiveRecordCollection
     */
    public function all($db = null)
    {
        return new ActiveRecordCollection(parent::all($db));
    }

}
