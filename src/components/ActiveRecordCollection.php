<?php

namespace yiiaddon\components;

use yiiaddon\helpers\ArrayHelper;
use yiiaddon\helpers\ArrayObject;

/**
 * A collection of active records when getting data from the database
 *
 * @category  PHP
 * @package   adeattwood\yii-addon
 * @author    Ade Attwood <hello@adeattwood.co.uk>
 * @copyright 2017 adeattwood.co.uk
 * @license   BSD-2-Clause http://adeattwood.co.uk/license.html
 * @link      adeattwood.co.uk
 * @since     v1.2
 */
class ActiveRecordCollection extends \yiiaddon\helpers\ArrayObject
{

    /**
     * Maps a the collection into a single key-value pair
     *
     * ~~~php
     * $model = Model::find()->all();
     * $map = $model->map('id', 'name');
     * ~~~
     *
     * @param string $key   The attribute you want to be the key
     * @param string $value The attribute you want to be the value
     *
     * @return ArrayObject
     */
    public function map($key, $value)
    {
        return new ArrayObject(ArrayHelper::map((array) $this, $key, $value));
    }

}
